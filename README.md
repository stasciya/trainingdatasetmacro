# TrainingDatasetMacro

This is intended to be used to generate training dataset from many TIFF files to be used for ilastik. 

You can select the folder, where the TIFF files are in, number of frames to be extracted and where to save the new HDF5 file.

One single HDF5 file will be generated for training, which makes it easier to train ilastik, without switching between images.

Samples are collected in regular intervalls. For instance, if you have a hyperstack with 100 frames and you choose to extract 3, then frames 1, 50 and 100 are going to be extracted.

saren.tasciyan@ist.ac.at
