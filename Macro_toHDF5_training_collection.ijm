dir = "";

nFrames = 5;
hdf5Name = "training.h5"

// Get directory
while(dir == "" || !File.isDirectory(dir)){
	dir = getDirectory(dir);
	if(dir == "" || !File.isDirectory(dir)){
		quit = getBoolean("You didn't seem to have chosen a valid directory. Do you want to quit?");	
		if(quit){
			exit;
		}
	}
}
print("Dir: " + dir);

// How many frames need to be sampled from each stack?
nFrames = getNumber("How many frames do you want to collect?", nFrames);

// Get file name for HDF5 file
fileNameCheck = false; replaceFile = false;
while(!fileNameCheck){
	hdf5Name = getString("Name of HDF5 file name?", hdf5Name);
	if(File.exists(dir + hdf5Name)){
		replaceFile = getBoolean("File with this name seems to exist already. Do you want to replace it?");
		if(replaceFile){
			fileNameCheck = true;
		}
	}else{
		fileNameCheck = true;
	}
	
}

batchMode = getBoolean("Batch mode?");

setBatchMode(batchMode);
files = getFileList(dir);
imageInd = 0; // f is index of files but not all files are TIFF files. We need to keep track of TIFF index.
for(f = 0; f < lengthOf(files); f++){
	file = files[f];
	print(dir + file);
	if(endsWith(file, ".tif") || endsWith(file, ".tiff")){
		// We open as virtual stack as there is no need to load the whole file at this moment. This increases performace for large files.
		run("TIFF Virtual Stack...", "open=[" + dir + file + "]");
		virtImageID = getImageID();
		getDimensions(width, height, channels, slices, frames);
		framesToIncludeArr = newArray(nFrames);
		framesToIncludeArr[0] = 1; framesToIncludeArr[lengthOf(framesToIncludeArr) - 1] = frames;
		for(i = 1; i < nFrames - 1; i++){
			framesToIncludeArr[i] = round((frames / (nFrames - 1)) * i);
		}
		Array.print(framesToIncludeArr);
		
		run("Make Substack...", "channels=1-" + toString(channels) + " frames=" + String.join(framesToIncludeArr, ","));

		// Appending samples...
		if(imageInd == 0){
			rename(hdf5Name);
		}else{
			run("Concatenate...", "open title=[" + hdf5Name + "] image1=[" + hdf5Name + "] image2=[" + getTitle() + "]");
			print("conc");
		}
		selectImage(virtImageID); close();
		print(getTitle());
		showProgress(f, lengthOf(files) - 1);
		imageInd++;
	}
}

run("Export HDF5", "select=[" + dir + hdf5Name + "] exportpath=[" + dir + hdf5Name + "] datasetname=data compressionlevel=9 input=[" + hdf5Name + "]");

close();
setBatchMode(false);
